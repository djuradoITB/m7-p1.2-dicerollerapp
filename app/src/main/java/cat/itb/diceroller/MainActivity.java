package cat.itb.diceroller;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private ImageView resultview;
    private ImageView resultview2;
    private Button rollButton;
    private Button clearButton;
    private int num;

    private int[] daus = {
            R.drawable.dice_1,
            R.drawable.dice_2,
            R.drawable.dice_3,
            R.drawable.dice_4,
            R.drawable.dice_5,
            R.drawable.dice_6,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        resultview = findViewById(R.id.roll1_result_view);
        resultview2 = findViewById(R.id.roll2_result_view);
        rollButton = findViewById(R.id.roll_button);
        clearButton = findViewById(R.id.clear_button);

        rollButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "He apretat el botó", Toast.LENGTH_SHORT).show();
                rollButton.setText(R.string.clicked_button);

                int randnum1 = genRandNum();
                int randnum2 = genRandNum();
                if (jackpot(randnum1, randnum2)) {
                    Toast jackpotToast = Toast.makeText(MainActivity.this, "JACKPOT!", Toast.LENGTH_SHORT);
                    jackpotToast.setGravity(Gravity.TOP,0, 0);
                    jackpotToast.show();
                }
                resultview.setImageResource(daus[randnum1]);
                resultview2.setImageResource(daus[randnum2]);
            }
        });

        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recreate();

                /*
                * He utilitzat aquest mètode per aprofitar la API però de manera manual es podria fer amb:
                *
                * rollButton.setText(R.string.button_text);
                * resultview.setImageResource(R.drawable.empty_dice);
                * resultview2.setImageResource(R.drawable.empty_dice);
                *
                * Poc òptim per a llargs programes
                * */
            }
        });

        resultview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rollSingleDice(resultview);
            }
        });

        resultview2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rollSingleDice(resultview2);
            }
        });
    }

    private int genRandNum() {
        return (int) (Math.random() * 6);
    }


    private void rollSingleDice(ImageView dice) {
        dice.setImageResource(daus[genRandNum()]);
    }

    private boolean jackpot(int n, int n2) {
        final int NUM_JACKPOT = 6 - 1;
        if (n == NUM_JACKPOT && n2 == NUM_JACKPOT) return true;
        else return false;
    }
}